1
00:00:00,160 --> 00:00:04,720
another dynamic analysis option that's

2
00:00:02,240 --> 00:00:05,759
available to you is address sanitizer or

3
00:00:04,720 --> 00:00:08,639
asan

4
00:00:05,759 --> 00:00:11,200
a-san and the kernel version k-san

5
00:00:08,639 --> 00:00:13,360
are things that modify the compilation

6
00:00:11,200 --> 00:00:15,200
significantly in order to try to detect

7
00:00:13,360 --> 00:00:16,640
things like buffer overflows use after

8
00:00:15,200 --> 00:00:18,240
freeze and some other vulnerability

9
00:00:16,640 --> 00:00:21,439
classes

10
00:00:18,240 --> 00:00:23,760
now asan is compile time additions to

11
00:00:21,439 --> 00:00:24,640
the code to add a bunch of extra sanity

12
00:00:23,760 --> 00:00:27,439
checks

13
00:00:24,640 --> 00:00:29,920
but it also includes modifications via

14
00:00:27,439 --> 00:00:32,239
library to make the tracking of

15
00:00:29,920 --> 00:00:34,000
dynamically allocated memory a lot more

16
00:00:32,239 --> 00:00:35,920
fine-grained so that it can detect if

17
00:00:34,000 --> 00:00:38,079
there's any sort of vulnerabilities in

18
00:00:35,920 --> 00:00:40,000
the handling of the heap memory in

19
00:00:38,079 --> 00:00:42,879
addition to significant compile-time

20
00:00:40,000 --> 00:00:45,440
modifications of the code asan also

21
00:00:42,879 --> 00:00:48,079
includes memory allocation library

22
00:00:45,440 --> 00:00:50,719
changes so that it can detect any at a

23
00:00:48,079 --> 00:00:52,160
very fine grain any sort of corruption

24
00:00:50,719 --> 00:00:54,320
of the data

25
00:00:52,160 --> 00:00:56,640
adjacent to memory allocations made by

26
00:00:54,320 --> 00:00:58,399
malloc so any sort of heap overflow

27
00:00:56,640 --> 00:01:01,359
would be caught as well

28
00:00:58,399 --> 00:01:03,840
now unfortunately asan has significant

29
00:01:01,359 --> 00:01:06,720
performance penalty code size penalty

30
00:01:03,840 --> 00:01:09,280
and memory size penalty so asan is not

31
00:01:06,720 --> 00:01:11,360
meant to be run in production code but

32
00:01:09,280 --> 00:01:14,159
instead is meant to be run inside of a

33
00:01:11,360 --> 00:01:16,159
testing or qa type environment so

34
00:01:14,159 --> 00:01:18,640
assuming that you actually do tests on

35
00:01:16,159 --> 00:01:21,600
your code before you ship them out the

36
00:01:18,640 --> 00:01:24,240
door asan should be enabled as part of

37
00:01:21,600 --> 00:01:26,400
the testing or qa environment

38
00:01:24,240 --> 00:01:28,560
now asan actually works extremely well

39
00:01:26,400 --> 00:01:31,119
with fuzzers because asan is only as

40
00:01:28,560 --> 00:01:33,520
good as the coverage tests that are

41
00:01:31,119 --> 00:01:35,119
available to it so again if you want to

42
00:01:33,520 --> 00:01:37,520
find this kind of thing dynamically you

43
00:01:35,119 --> 00:01:39,600
have to provide inputs to the code in

44
00:01:37,520 --> 00:01:42,240
order to see if there's anything going

45
00:01:39,600 --> 00:01:44,000
wrong and assuming that your normal qa

46
00:01:42,240 --> 00:01:45,920
or test coverage doesn't actually have

47
00:01:44,000 --> 00:01:48,960
completely programmatically randomized

48
00:01:45,920 --> 00:01:51,680
inputs well just turning on asan by

49
00:01:48,960 --> 00:01:53,200
itself probably is not going to catch

50
00:01:51,680 --> 00:01:55,360
the sort of

51
00:01:53,200 --> 00:01:57,439
significant bugs that would be caused by

52
00:01:55,360 --> 00:01:59,600
attacker-controlled input if on the

53
00:01:57,439 --> 00:02:02,079
other hand instead of using your typical

54
00:01:59,600 --> 00:02:04,479
qa test suite you use something like a

55
00:02:02,079 --> 00:02:06,240
coverage GUIDed fuzzer well then you

56
00:02:04,479 --> 00:02:08,000
know basically the fuzzer is helping

57
00:02:06,240 --> 00:02:10,560
force the code down a bunch of different

58
00:02:08,000 --> 00:02:12,239
paths and then asan is keeping a close

59
00:02:10,560 --> 00:02:14,560
eye on any sort of corruption that can

60
00:02:12,239 --> 00:02:16,560
occur and then it provides you nice

61
00:02:14,560 --> 00:02:18,800
detailed information about you know back

62
00:02:16,560 --> 00:02:21,360
traces of how you got to that particular

63
00:02:18,800 --> 00:02:23,760
control flow unfortunately asan doesn't

64
00:02:21,360 --> 00:02:25,120
play nice with another mitigation and

65
00:02:23,760 --> 00:02:27,040
detection technique that we learned

66
00:02:25,120 --> 00:02:29,520
about fortify source

67
00:02:27,040 --> 00:02:31,440
so you know the the fact says currently

68
00:02:29,520 --> 00:02:32,959
you can't use both at the same time so

69
00:02:31,440 --> 00:02:35,440
unfortunately you have to turn off

70
00:02:32,959 --> 00:02:37,920
fortify source in order to use acet

71
00:02:35,440 --> 00:02:40,480
so in general you know the the monocle

72
00:02:37,920 --> 00:02:43,680
emoji says run fortify source in

73
00:02:40,480 --> 00:02:46,800
production run asan in the debug or qa

74
00:02:43,680 --> 00:02:46,800
test environment

